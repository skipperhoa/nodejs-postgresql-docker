CREATE TABLE tutorials (
    id SERIAL PRIMARY KEY,
    title character varying(255) NOT NULL UNIQUE,
    description character varying(255) NOT NULL,
    published BOOLEAN NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

INSERT INTO tutorials (id, title, description,published) VALUES 
(1, 'There are only two kinds of languages: the ones people complain about and the ones nobody uses.', 'Bjarne Stroustrup',true), 
(2, 'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.', 'Martin Fowler',true);